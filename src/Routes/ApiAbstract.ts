import express, {Router} from "express";

/**
 * API abstract class. Initializes Express router
 */
export default abstract class ApiAbstract {
    /**
     * Express router
     */
    public readonly router: Router;

    /**
     * API endpoint
     */
    public readonly endpoint: string;

    /**
     * Constructor. Initializes Express router
     */
    constructor(endpoint: string) {
        this.endpoint = endpoint;
        this.router = express.Router();
    }
}