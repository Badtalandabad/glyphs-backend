import GlyphsController from "../Core/Modules/Glyphs/GlyphsController" ;
import ApiAbstract from "./ApiAbstract";
import Application from "../Core/Application";

/**
 * API version 1 routes
 */
export default class ApiV1 extends ApiAbstract {
    /**
     * Constructor. Initializes Express router.
     */
    constructor(app: Application) {
        super("/api/v1");

        this.initGlyphRoutes(app);
    }

    /**
     * Initializes Glyph controller routes
     */
    private initGlyphRoutes(app: Application) {
        const glyphsController = new GlyphsController(app);

        this.router.post("/get-glyphs", glyphsController.getGlyphs.bind(glyphsController));
        this.router.post("/save-glyph", glyphsController.saveGlyph.bind(glyphsController));
    }
}
