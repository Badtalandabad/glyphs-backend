import DbDataAbstract from "../../Db/DbDataAbstract";
import SystemError from "../../Library/SystemError";

/**
 * Database glyph entity
 */
export default class GlyphData extends DbDataAbstract {
    symbol: string;

    pinyin: string;

    meaning: string;

    public constructor(rawData: Record<string, unknown>) {
        super(rawData);

        if (typeof rawData.symbol !== "string"
            || typeof rawData.pinyin !== "string"
            || typeof rawData.meaning !== "string"
        ) {
            throw new SystemError("Raw data for Glyph class is incorrect");
        }

        this.symbol = rawData.symbol;
        this.pinyin = rawData.pinyin;
        this.meaning = rawData.meaning;
    }
}