export default {
    WRONG_TYPE_GLYPH: "'glyph' parameter must be a string.",
    WRONG_TYPE_PINYIN: "'pinyin' parameter must be a string.",
    WRONG_TYPE_MEANING: "'meaning' parameter must be a string.",

    NOT_VALID_GLYPH: "Glyph must be just one character.",
    NOT_VALID_PINYIN: "Provided pinyin is not valid.",
    NOT_VALID_MEANING: "Meaning of the glyph must be not an empty string.",

    DUPLICATE_GLYPH: "This glyph was already saved before.",
    DB_PROBLEM: "Can't save the glyph, something went wrong with database.",
};