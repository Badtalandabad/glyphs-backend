import {Request, response, Response} from "express";

import GlyphsModel from "./GlyphsModel";
import ControllerAbstract from "../../Library/ControllerAbstract";
import Application from "../../Application";
import GlyphData from "./GlyphData";
import CommandResult from "../../Library/CommandResult";
import errorCodes from "./errorCodes";

/**
 * Glyphs controller
 */
export default class GlyphsController extends ControllerAbstract {
    /**
     * Glyphs model
     */
    private model: GlyphsModel;

    /**
     * Constructor. Initialize model
     */
    public constructor(app :Application) {
        super(app);

        this.model = new GlyphsModel(app.db);
    }

    /**
     * get-glyphs API method. Retrieves all glyphs from db
     *
     * @param req Express request
     * @param res Express response
     */
    public getGlyphs(req: Request, res: Response): void {
        if (typeof req.body.glyphsCount !== "number"
            || req.body.glyphsCount < 1
            || !Number.isInteger(req.body.glyphsCount)
        ) {
            const errorMessage = "'glyphsCount' parameter must be a valid positive integer number.";
            this.responseHandler.respondBadRequest(res, errorMessage);
            return;
        }
        const glyphsCount: number = req.body.glyphsCount;

        this.model.getGlyphs(glyphsCount).then((result: GlyphData[]) => {
            this.responseHandler.respondOk(res, result, "Success");
        }).catch((error: Error) => this.errorHandler.finishRequestWithError(error, req, res));
    }

    /**
     * save-glyph action. Saves new glyph in db. Checks if it already exists beforehand
     *
     * @param req Express request
     * @param res Express response
     */
    public saveGlyph(req: Request, res: Response): void {
        let errorMessage = "";
        if (typeof req.body.glyph !== "string") {
            errorMessage += errorCodes.WRONG_TYPE_GLYPH + " ";
        }
        const glyph: string = req.body.glyph;

        if (typeof req.body.pinyin !== "string") {
            errorMessage += errorCodes.WRONG_TYPE_PINYIN + " ";
        }
        const pinyin: string = req.body.pinyin;

        if (typeof req.body.meaning !== "string") {
            errorMessage += errorCodes.WRONG_TYPE_MEANING;
        }
        const meaning: string = req.body.meaning;

        if (errorMessage) {
            this.responseHandler.respondBadRequest(res, errorMessage.trim());
            return;
        }

        this.model.saveGlyph(glyph, pinyin, meaning)
            .then((result: CommandResult) => {
                if (result.success) {
                    this.responseHandler.respondOk(res, {}, "Saved successfully.");
                } else if (result.isClientError) {
                    this.responseHandler.respondBadRequest(res, result.message);
                } else {
                    this.responseHandler.respondError(res, result.message)
                }
            }).catch((error: Error) => this.errorHandler.finishRequestWithError(error, req, res));
    }
}