import ModelAbstract from "../../Library/ModelAbstract";
import GlyphData from "./GlyphData";
import GlyphsRepository from "./GlyphsRepository";
import Db from "../../Db/Db";
import CommandResult from "../../Library/CommandResult";
import errorCodes from "./errorCodes";
import PinyinValidation from "../../Library/Validation/PinyinValidation";
import SystemError from "../../Library/SystemError";

/**
 * Glyphs model
 */
export default class GlyphsModel extends ModelAbstract {
    private glyphsRepository: GlyphsRepository;

    constructor(db: Db) {
        super();

        this.glyphsRepository = new GlyphsRepository(db);
    }

    /**
     * Retrieves glyphs from database
     */
    public getGlyphs(glyphsCount: number): Promise<GlyphData[]> {
        return this.glyphsRepository.getGlyphs(glyphsCount);
    }

    public saveGlyph(glyph: string, pinyin: string, meaning: string): Promise<CommandResult> {
        const actionResult = new CommandResult(false);

        const pinyinValidation = new PinyinValidation();
        if (glyph.length !== 1) {
            actionResult.message = errorCodes.NOT_VALID_GLYPH;
        }
        if (!pinyinValidation.validate(pinyin)) {
            actionResult.message = errorCodes.NOT_VALID_PINYIN;
        }
        if (!meaning) {
            actionResult.message = errorCodes.NOT_VALID_MEANING;
        }
        if (actionResult.message) {
            return Promise.resolve(actionResult);
        }

        return this.glyphsRepository.getGlyph(glyph).then(result => {
            if (result.length) {
                actionResult.message = errorCodes.DUPLICATE_GLYPH;
                return actionResult;
            }

            return this.glyphsRepository.saveGlyph(glyph, pinyin, meaning)
                .then(result => {
                    if (result) {
                        actionResult.success = true;
                        return actionResult;
                    }
                    throw new SystemError(errorCodes.DB_PROBLEM);
                });
        });


    }
}