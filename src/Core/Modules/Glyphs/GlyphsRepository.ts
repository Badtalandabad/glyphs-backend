import RepositoryAbstract from "../../Library/RepositoryAbstract";
import GlyphData from "./GlyphData";
import SystemError from "../../Library/SystemError";

/**
 * Repository class. Covers interaction with database
 */
export default class GlyphsRepository extends RepositoryAbstract {
    /**
     * Table name
     */
    public readonly glyphsTable = "glyphs";

    /**
     * Makes glyph objects from raw database records array
     *
     * @param data Raw database object
     */
    private makeGlyphsFromRawData(data: Record<string, unknown>[]): GlyphData[] {
        return data.map(glyphRawData => new GlyphData(glyphRawData));
    }

    /**
     * Retrieves information of one glyph
     *
     * @param glyph Searched glyph
     */
    public getGlyph(glyph: string): Promise<GlyphData[]> {
        const sql = `SELECT * FROM ${this.glyphsTable} WHERE symbol = ?;`;

        return this.db.query(sql, [glyph])
            .then(glyphsData => {
                if (!Array.isArray(glyphsData)) {
                    throw new SystemError("Inconsistent data. Got " + typeof glyphsData + " instead of array");
                }
                return this.makeGlyphsFromRawData(glyphsData);
            });
    }

    /**
     * Retrieves named amount of glyphs
     *
     * @param glyphsCount Number of glyphs to retrieve
     */
    public getGlyphs(glyphsCount: number, random = true): Promise<GlyphData[]> {
        const order = random ? ' ORDER BY RAND()' : '';
        const sql = `SELECT * FROM ${this.glyphsTable} ${order} LIMIT ?;`;

        return this.db.query(sql, [glyphsCount])
            .then(glyphsData => {
                if (!Array.isArray(glyphsData)) {
                    throw new SystemError("Inconsistent data. Got " + typeof glyphsData + " instead of array");
                }
                return this.makeGlyphsFromRawData(glyphsData);
            });
    }

    /**
     * Saves glyph information
     *
     * @param glyph   Glyph, on character
     * @param pinyin  Pinyin of the glyph
     * @param meaning Meaning of the glyph
     */
    public saveGlyph(glyph: string, pinyin: string, meaning: string): Promise<boolean> {
        const insertSql = `INSERT INTO ${this.glyphsTable} SET symbol = ?, pinyin = ?, meaning = ?;`;

        return this.db.query(insertSql, [glyph, pinyin, meaning]).then(result => {
            if (result.affectedRows === undefined) {
                throw new SystemError("Got " + typeof result + " instead of OkPacket");
            }
            return !!result.affectedRows;
        });
    }
}