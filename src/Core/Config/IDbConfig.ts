interface IDbConfig {
    host: string;
    port: number;
    user: string;
    password: string;
    database: string;
}

export default IDbConfig;