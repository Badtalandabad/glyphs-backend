import IAppConfig from "./IAppConfig";
import IDbConfig from "./IDbConfig";

interface IConfig {
    app: IAppConfig;
    db: IDbConfig;
}

export default IConfig;