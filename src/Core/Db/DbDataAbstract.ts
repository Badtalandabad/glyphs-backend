import SystemError from "../Library/SystemError";

/**
 * Represents db data entity. Declares common fields for all data entities.
 */
export default abstract class DbDataAbstract {
    /**
     * Entity ID
     */
    id: number;

    /**
     * Date and time when entity was created
     */
    createdOn: Date;

    /**
     * Date and time when entity was updated last time
     */
    updatedOn: Date;

    /**
     * Constructor. Inits basic data
     *
     * @param rawData Raw data object
     */
    public constructor(rawData: Record<string, unknown>) {
        if (typeof rawData.id !== "number"
            || !(rawData.created_on instanceof Date)
            || !(rawData.updated_on instanceof Date)
        ) {
            throw new SystemError("Raw data is inconsistent with data class");
        }

        this.id = rawData.id;
        this.createdOn = rawData.created_on;
        this.updatedOn = rawData.updated_on;
    }
}