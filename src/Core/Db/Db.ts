import {createConnection, Connection} from "mysql";

import IDbConfig from "../Config/IDbConfig";
import SystemError from "../Library/SystemError";

/**
 * Database management layer
 */
export default class Db {
    /**
     * Database connection object
     */
    private connection: Connection;

    /**
     * Flag to allow database usage by the application
     */
    private _dbUsageAllowed = false;

    /**
     * Returns true if application database usage is allowed, false otherwise
     */
    get dbUsageAllowed(): boolean {
        return this._dbUsageAllowed;
    }

    /**
     * Turning on/off application database usage
     *
     * @param allowed boolean True if database usage allowed, false if not
     */
    set dbUsageAllowed(allowed: boolean) {
        if (allowed) {
            console.log("Opening database access");
        } else {
            console.log("Closing database access");
        }
        this._dbUsageAllowed = allowed;
    }

    /**
     * Db constructor
     *
     * @param dbConfig IDbConfig Database part of configuration
     */
    constructor(dbConfig: IDbConfig) {
        this.connection = createConnection(dbConfig);

        this.connection.connect((error) => {
            if (error) {
                //TODO: register error
                console.error(`Db connection failed: ${error.stack}`);
                return;
            }
            console.log(`Connected to db: thread id ${this.connection.threadId}`);
            this.dbUsageAllowed = true;
        });
    }

    /**
     * Does database request
     *
     * @param query  SQL query with placeholders (? or named ones)
     * @param params Parameters for placeholders in array or object (named parameters)
     */
    public query(
        query: string,
        params: Array<string|number> | Record<string, string|number> = []
    ): Promise<Record<string, unknown>> {
        if (!this.dbUsageAllowed) {
            throw new SystemError("Attempt to use database before connection established");
        }
        const queryCallback = (resolve: (value: Record<string, unknown>) => void, reject: (error: Error) => void) => {
            this.connection.query(query, params, (error, result) => {
                if (error) {
                    //TODO: register error
                    reject(error);
                }
                resolve(result);
            });
        };

        return new Promise(queryCallback);

    }
}