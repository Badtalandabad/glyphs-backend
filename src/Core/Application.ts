import express, {NextFunction, Request, Response} from "express";
import cors from "cors";
import cookieParser from "cookie-parser";
import morgan from "morgan";

import IConfig from "./Config/IConfig";
import IDbConfig from "./Config/IDbConfig";
import Db from "./Db/Db";
import ApiV1 from "../Routes/ApiV1";
import ApiAbstract from "../Routes/ApiAbstract";
import SystemError from "./Library/SystemError";
import ErrorHandler from "./Library/Response/ErrorHandler";
import ResponseHandler from "./Library/Response/ResponseHandler";

/**
 * Main application class
 */
export default class Application {
    /**
     * Express instance
     */
    private express: express.Application;

    /**
     * Application configuration
     */
    private config: IConfig;

    /**
     * Database management object
     */
    public db: Db;

    /**
     * API object. Includes routes. Version may be changed depending on need
     */
    public api: ApiAbstract;

    /**
     * Handles http response to the client
     */
    public responseHandler: ResponseHandler;

    /**
     * Handles errors: logs, sends error message using response handler
     */
    public errorHandler: ErrorHandler;

    /**
     * Application constructor. Basic application initialization
     *
     * @param config IConfigFile Application configuration
     */
    public constructor(config: IConfig) {
        this.config = config;
        this.express = express();

        this.responseHandler = new ResponseHandler();
        this.errorHandler = new ErrorHandler(this.responseHandler);

        this.db = this.makeDbConnection(this.config.db);
        this.api = new ApiV1(this);

        this.setUpMiddleware();
    }

    /**
     * Sets up all middleware
     */
    public setUpMiddleware(): void {
        if (process.env.NODE_ENV === "development") {
            this.express.use(cors());
            this.express.use(morgan("dev"));
        }
        this.express.use(express.json());
        this.express.use(express.urlencoded({ extended: true }));
        this.express.use(cookieParser());
        this.express.use(this.api.endpoint, this.api.router);
        this.express.use((req, res, next) => {
            const error = new SystemError("Not Found");
            error.status = 404;
            next(error);
        });
        this.express.use((error: Error, req: Request, res: Response, next: NextFunction) => {
            this.errorHandler.finishRequestWithError(error, req, res);
            next();
        });
    }

    /**
     * Establishes connection to database
     */
    public makeDbConnection(dbConfig: IDbConfig): Db {
        const host     = dbConfig.host;
        const port     = dbConfig.port;
        const password = dbConfig.password;
        const user     = dbConfig.user;
        const database = dbConfig.database;

        return new Db({host, port, user, password, database});
    }

    /**
     * Runs application. Starts listening for incoming requests
     *
     * @param port number Port to listen on
     */
    public run(port: number): void {

        this.express.listen(port, () => {
            console.log(`App is listening on port ${port}!`);
        });
    }
}
