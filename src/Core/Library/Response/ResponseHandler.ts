/**
 * Sends response to client
 */
import {Response} from "express";

import ClientResponse from "./ClientResponse";

export default class ResponseHandler {
    /**
     * Responds to client in json format
     *
     * @param expressResponse Express.js response object
     * @param httpStatusCode  Http status code
     * @param requestResult   Result data of the request. Optional
     * @param message         Additional message in response json. Optional
     */
    respondToClient<T>(
        expressResponse: Response,
        httpStatusCode: number,
        requestResult: T | Record<string, never> = {},
        message = ""
    ): void {
        expressResponse.status(httpStatusCode);

        expressResponse.json(new ClientResponse<T>(requestResult, message));
    }

    /**
     * Responds with 200 status
     *
     * @param expressResponse Express.js response object
     * @param requestResult   Result data of the request. Optional
     * @param message         Message. Optional
     */
    respondOk<T>(expressResponse: Response, requestResult?: T, message?: string): void {
        this.respondToClient(expressResponse, 200, requestResult, message);
    }

    /**
     * Responds with 400 status. Bad request
     *
     * @param expressResponse Express.js response object
     * @param message         Error message
     */
    respondBadRequest(expressResponse: Response, message: string): void {
        this.respondToClient(expressResponse, 400, undefined, message);
    }

    /**
     * Responds with 500 status. Server error
     *
     * @param expressResponse Express.js response object
     * @param message         Error message
     */
    respondError(expressResponse: Response, message: string): void {
        this.respondToClient(expressResponse, 500, undefined, message);
    }

}