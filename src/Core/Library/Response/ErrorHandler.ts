import {Request, Response} from "express";

import ResponseHandler from "./ResponseHandler";
import SystemError from "../SystemError";

/**
 * Error Handler. Processes and reports errors.
 */
export default class ErrorHandler {
    /**
     * Response handler
     */
    responseHandler: ResponseHandler;

    /**
     * Logs error
     *
     * @param error Error object
     */
    logError(error: Error): void {
        console.error(error.stack);
    }

    /**
     * Constructor. Instantiates Response handler
     */
    constructor(responseHandler: ResponseHandler) {
        this.responseHandler = responseHandler;
    }

    /**
     * Logs error, sends general error message with status 500
     * Is an arrow function in order to make it suitable as middleware
     *
     * @param error Error object
     * @param req   Express.js request object
     * @param res   Express.js response object
     */
    finishRequestWithError(error: Error, req: Request, res: Response): void {
        res.locals.error = error;

        let status = 500;
        if (error instanceof SystemError) {
            status = error.status;
        }
        this.logError(error);

        let message = "Error";
        switch (status) {
            case 404:
                message = "Not found";
                break;
            case 500:
                message = "Internal server error";
                break;
        }

        this.responseHandler.respondToClient(res, status, null, message);
    }
}