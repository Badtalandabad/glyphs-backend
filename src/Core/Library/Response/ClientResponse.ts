export default class ClientResponse<T> {
    public data: T | Record<string, never>;
    public message: string;

    constructor(data: T | Record<string, never>, message: string) {
        this.data = data;
        this.message = message;
    }
}