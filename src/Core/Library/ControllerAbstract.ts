import ResponseHandler from "./Response/ResponseHandler";
import Application from "../Application";
import ErrorHandler from "./Response/ErrorHandler";

/**
 * Abstract controller class
 */
export default abstract class ControllerAbstract {
    /**
     * Response handler getter
     */
    get responseHandler(): ResponseHandler {
        return this.app.responseHandler;
    }

    get errorHandler(): ErrorHandler {
        return this.app.errorHandler;
    }

    /**
     * Application instance
     */
    public app: Application;

    /**
     * Constructor. Instantiates response handler for every controller
     *
     * @param app Application instance
     */
    public constructor(app: Application) {
        this.app = app;
    }
}