/**
 * First part of pinyin variants.
 * Caution, in order to make it more simple initials here don't reflect
 * exact all possible initials of pinyin
 */
const i: Record<string, string> = {
    zh:"zh",
    ch:"ch",
    sh:"sh",
    y: "y",
    w: "w",
    b: "b",
    p: "p",
    m: "m",
    f: "f",
    d: "d",
    t: "t",
    n: "n",
    l: "l",
    g: "g",
    k: "k",
    h: "h",
    j: "j",
    q: "q",
    x: "x",
    z: "z",
    c: "c",
    s: "s",
    r: "r",
}

/**
 * Last part of pinyin variants.
 * Caution, in order to make it more simple finals here don't reflect
 * exact all possible finals of pinyin
 */
const f: Record<string, string> = {
    a:   "a",
    ai:  "ai",
    an:  "an",
    ang: "ang",
    ao:  "ao",
    e:   "e",
    ei:  "ei",
    en:  "en",
    eng: "eng",
    er:  "er",
    i:   "i",
    ia:  "ia",
    ian: "ian",
    iang:"iang",
    iao: "iao",
    ie:  "ie",
    in:  "in",
    ing: "ing",
    iong:"iong",
    iu:  "iu",
    o:   "o",
    ong: "ong",
    ou:  "ou",
    u:   "u",
    ua:  "ua",
    uai: "uai",
    uan: "uan",
    uang:"uang",
    ui:  "ui",
    un:  "un",
    uo:  "uo",
    yu:  "ü",
    ue:  "ue",
    yue: "üe",
}

const toneChars: Record<string, string> = {
    "ā": "a",
    "á": "a",
    "ǎ": "a",
    "à": "a",
    "ī": "i",
    "í": "i",
    "ǐ": "i",
    "ì": "i",
    "ō": "o",
    "ó": "o",
    "ǒ": "o",
    "ò": "o",
    "ē": "e",
    "é": "e",
    "ě": "e",
    "è": "e",
    "ū": "u",
    "ú": "u",
    "ǔ": "u",
    "ù": "u",
    "ǖ": "ü",
    "ǘ": "ü",
    "ǚ": "ü",
    "ǜ": "ü",
}

export default class PinyinValidation {
    private rules: Record<string, string[]> = {
        [""]:  [f.a, f.ai, f.an, f.ang, f.ao, f.e, f.ei, f.en, f.eng, f.er, f.o, f.ou,],
        [i.y]: [f.i, f.a, f.an, f.ang, f.ao, f.e, f.in, f.ing, f.ong, f.ou, f.u, f.uan, f.ue, f.un,],
        [i.w]: [f.u, f.a, f.ai, f.an, f.ang, f.ei, f.en, f.eng, f.o,],
        [i.b]: [f.a, f.ai, f.an, f.ang, f.ao, f.ei, f.en, f.eng, f.i, f.ian, f.iao, f.ie, f.in, f.ing, f.o, f.u],
        [i.p]: [f.a, f.ai, f.an, f.ang, f.ao, f.ei, f.en, f.eng, f.i, f.ian, f.iao, f.ie, f.in, f.ing, f.o, f.ou, f.u],
        [i.m]: [f.a, f.ai, f.an, f.ang, f.ao, f.e, f.ei, f.en, f.eng, f.i, f.ian, f.iao, f.ie, f.in, f.ing, f.iu, f.o,
            f.ou, f.u],
        [i.f]: [f.a, f.an, f.ang, f.ei, f.en, f.eng, f.o, f.ou, f.u],
        [i.d]: [f.a, f.ai, f.an, f.ang, f.ao, f.e, f.ei, f.eng, f.i, f.ia, f.ian, f.iao, f.ie, f.ing, f.iu, f.ong,
            f.ou, f.u, f.uan, f.ui, f.un, f.uo,],
        [i.t]: [f.a, f.ai, f.an, f.ang, f.ao, f.e, f.eng, f.i, f.ian, f.iao, f.ie, f.ing, f.ong, f.ou, f.u, f.uan,
            f.ui, f.un, f.uo,],
        [i.n]: [f.a, f.ai, f.an, f.ang, f.ao, f.e, f.ei, f.en, f.eng, f.i, f.ian, f.iang, f.iao, f.ie, f.in, f.ing,
            f.iu, f.ong, f.u, f.uan, f.uo, f.yu, f.yue],
        [i.l]: [f.a, f.ai, f.an, f.ang, f.ao, f.e, f.ei, f.eng, f.i, f.ia, f.ian, f.iang, f.iao, f.ie, f.in, f.ing,
            f.iu, f.ong, f.ou, f.u, f.uan, f.un, f.uo, f.yu, f.yue],
        [i.g]: [f.a, f.ai, f.an, f.ang, f.ao, f.e, f.ei, f.en, f.eng, f.ong, f.ou, f.u, f.ua, f.uai, f.uan, f.uang,
            f.ui, f.un, f.uo,],
        [i.k]: [f.a, f.ai, f.an, f.ang, f.ao, f.e, f.en, f.eng, f.ong, f.ou, f.u, f.ua, f.uai, f.uan, f.uang, f.ui,
            f.un, f.uo,],
        [i.h]: [f.a, f.ai, f.an, f.ang, f.ao, f.e, f.ei, f.en, f.eng, f.ong, f.ou, f.u, f.ua, f.uai, f.uan, f.uang,
            f.ui, f.un, f.uo,],
        [i.j]: [f.i, f.ia, f.ian, f.iang, f.iao, f.ie, f.in, f.ing, f.iong, f.iu, f.u, f.uan, f.ue, f.un],
        [i.q]: [f.i, f.ia, f.ian, f.iang, f.iao, f.ie, f.in, f.ing, f.iong, f.iu, f.u, f.uan, f.ue, f.un],
        [i.x]: [f.i, f.ia, f.ian, f.iang, f.iao, f.ie, f.in, f.ing, f.iong, f.iu, f.u, f.uan, f.ue, f.un],
        [i.z]: [f.i, f.a, f.ai, f.an, f.ang, f.ao, f.e, f.ei, f.en, f.eng, f.ong, f.ou, f.u, f.uan, f.ui, f.un, f.uo,],
        [i.c]: [f.i, f.a, f.ai, f.an, f.ang, f.ao, f.ei, f.en, f.eng, f.ong, f.ou, f.u, f.uan, f.ui, f.un, f.uo,],
        [i.s]: [f.i, f.a, f.ai, f.an, f.ang, f.ao, f.ei, f.en, f.eng, f.ong, f.ou, f.u, f.uan, f.ui, f.un, f.uo,],
        [i.zh]:[f.i, f.a, f.ai, f.an, f.ang, f.ao, f.e, f.ei, f.en, f.eng, f.ong, f.ou, f.u, f.ua, f.uai, f.uan,
            f.uang, f.ui, f.un, f.uo,],
        [i.ch]:[f.i, f.a, f.ai, f.an, f.ang, f.ao, f.e, f.en, f.eng, f.ong, f.ou, f.u, f.uai, f.uan, f.uang, f.ui,
            f.un, f.uo,],
        [i.sh]:[f.i, f.a, f.ai, f.an, f.ang, f.ao, f.e, f.ei, f.en, f.eng, f.ou, f.u, f.ua, f.uai, f.uan, f.uang,
            f.ui, f.un, f.uo,],
        [i.r]: [f.i, f.an, f.ang, f.ao, f.e, f.en, f.eng, f.ong, f.ou, f.u, f.uan, f.ui, f.un, f.uo],
    }

    public validate(pinyin: string): boolean {
        const toneCharsRegExp = new RegExp("[" + Object.keys(toneChars).join("") + "]");
        pinyin = pinyin.toLowerCase().replace(toneCharsRegExp, x => toneChars[x]);

        const initialRegExp = new RegExp("^" + Object.keys(i).join("|"));
        const initialsMatch = initialRegExp.exec(pinyin);
        let initial = "";
        if (initialsMatch) {
            initial = initialsMatch[0];
        }

        const final = pinyin.slice(initial.length);

        return this.rules[initial].includes(final);
    }
}
