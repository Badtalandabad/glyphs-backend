/**
 * Used for communication between different layers of application
 */
export default class CommandResult {
    data: Record<string, unknown>[] = [];

    success: boolean;

    message = "";

    isClientError = true;

    public constructor(result: boolean) {
        this.success = result;
    }
}