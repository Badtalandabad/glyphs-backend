import Db from "../Db/Db";

export default abstract class RepositoryAbstract {
    /**
     * Database object
     */
    public readonly db: Db;

    /**
     * Constructor
     *
     * @param db Application database object
     */
    constructor(db: Db) {
        this.db = db;
    }
}