import Application from "./Core/Application";
import config from "./Config/config.json";

const app = new Application(config);

app.run(config.app.port);